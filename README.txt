This module allows you to make use of the jCarousel jQuery plugin (http://sorgalla.com/jcarousel/).

Please checkout the branch associated with your Drupal version...

Drupal 5:
  DRUPAL-5

Drupal 6:
  DRUPAL-6--1 (1.x version)
  DRUPAL-6--2 (2.x version)

Drupal 7:
  DRUPAL-7--2
